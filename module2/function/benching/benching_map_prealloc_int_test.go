package benching

import (
	"testing"
)

// insertXPreallocIntMap применяется для добавления Х элементов в Map[int]int
func insertXPreallocIntMap(x int, b *testing.B) {
	// Инициализируем Map и вставляем X элементов
	testmap := make(map[int]int, x)
	b.ResetTimer()
	for i := 0; i < x; i++ {
		// Вставляем значение I в ключ I.
		testmap[i] = i
	}
}

// BenchmarkInsertIntMapPreAlloc100000 тестирует скорость вставки 100000 целых чисел в карту.
func BenchmarkInsertIntMapPrealloc100000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertXPreallocIntMap(100000, b)
	}
}

// BenchmarkInsertIntMapPreAlloc10000 тестирует скорость вставки 10000 целых чисел в карту.
func BenchmarkInsertIntMapPrealloc10000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertXPreallocIntMap(10000, b)
	}
}

// BenchmarkInsertIntMapPreAlloc1000 тестирует скорость вставки 1000 целых чисел в карту.
func BenchmarkInsertIntMapPrealloc1000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertXPreallocIntMap(1000, b)
	}
}

// BenchmarkInsertIntMapPreAlloc100 тестирует скорость вставки 100 целых чисел в карту.
func BenchmarkInsertIntMapPrealloc100(b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertXPreallocIntMap(100, b)
	}
}
