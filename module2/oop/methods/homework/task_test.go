package main

import (
	"testing"
)

func TestTask(t *testing.T) {
	tests := []struct {
		a   float64
		b   float64
		res []float64
	}{
		{
			a:   6,
			b:   5,
			res: []float64{30, 1.2, 11, 5.5},
		},
		{
			a:   12,
			b:   -3,
			res: []float64{-36, -4, 9, 4.5},
		},
		{
			a:   1,
			b:   1,
			res: []float64{1, 1, 2, 1},
		},
		{
			a:   2,
			b:   8,
			res: []float64{16, 0.25, 10, 5},
		},
		{
			a:   0,
			b:   4,
			res: []float64{0, 0, 4, 2},
		},
		{
			a:   -15,
			b:   -10,
			res: []float64{150, 1.5, -25, -12.5},
		},
		{
			a:   2.5,
			b:   4,
			res: []float64{10, 0.625, 6.5, 3.25},
		},
		{
			a:   0.4,
			b:   12.8,
			res: []float64{5.120000000000001, 0.03125, 13.200000000000001, 6.6000000000000005},
		},
	}

	testsFuncs := []struct {
		f    func(a, b float64) float64
		name string
	}{
		{
			f:    multiply,
			name: "multiply",
		},
		{
			f:    divide,
			name: "divide",
		},
		{
			f:    sum,
			name: "sum",
		},
		{
			f:    average,
			name: "average",
		},
	}

	calc := NewCalc()

	for _, tt := range tests {

		t.Run("", func(t *testing.T) {

			for i, testsF := range testsFuncs {

				if got := calc.SetA(tt.a).SetB(tt.b).Do(testsF.f).Result(); got != tt.res[i] {

					t.Errorf("%v = %v, res %v", testsF.name, got, tt.res[i])

				}

			}

		})

	}
}
