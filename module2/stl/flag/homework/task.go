package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
)

type Config struct {
	AppName    string `json:"appName"`
	Production bool   `json:"production"`
}

func main() {
	conf := Config{}
	dataJson := flag.String("conf", "", "config.json decoding")
	flag.Parse()
	data, err := os.ReadFile(*dataJson)
	if err != nil {
		fmt.Println(err)
	}
	err = json.Unmarshal(data, &conf)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(conf)
	}

	//fmt.Printf("%#v\n", conf)

}
