package main

import (
	"os"
	"unicode"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func translRusToEn(data string) string {
	conversions := []struct {
		rus string
		lat string
	}{
		{"а", "a"},
		{"А", "A"},
		{"б", "b"},
		{"Б", "B"},
		{"в", "v"},
		{"В", "V"},
		{"г", "g"},
		{"Г", "G"},
		{"д", "d"},
		{"Д", "D"},
		{"е", "e"},
		{"Е", "E"},
		{"ё", "yo"},
		{"Ё", "Yo"},
		{"ж", "zh"},
		{"Ж", "Zh"},
		{"з", "z"},
		{"З", "Z"},
		{"и", "i"},
		{"И", "I"},
		{"й", "j"},
		{"Й", "J"},
		{"к", "k"},
		{"К", "K"},
		{"л", "l"},
		{"Л", "L"},
		{"м", "m"},
		{"М", "M"},
		{"н", "n"},
		{"Н", "N"},
		{"о", "o"},
		{"О", "O"},
		{"п", "p"},
		{"П", "P"},
		{"р", "r"},
		{"Р", "R"},
		{"с", "s"},
		{"С", "S"},
		{"т", "t"},
		{"Т", "T"},
		{"у", "u"},
		{"У", "U"},
		{"ф", "f"},
		{"Ф", "F"},
		{"х", "h"},
		{"X", "H"},
		{"ц", "c"},
		{"Ц", "C"},
		{"ч", "ch"},
		{"Ч", "Ch"},
		{"ш", "sh"},
		{"Ш", "Sh"},
		{"щ", "shh"},
		{"Щ", "Shh"},
		{"ъ", ""},
		{"ы", "y"},
		{"Ы", "Y"},
		{"ь", ""},
		{"э", "e"},
		{"Э", "E"},
		{"ю", "yu"},
		{"Ю", "Yu"},
		{"я", "ya"},
		{"Я", "Ya"},
	}

	transl := make([]rune, len(data))

	for _, val := range data {
		for _, letter := range conversions {
			if !unicode.IsLetter(val) {
				transl = append(transl, val)
				break
			} else if string(val) == letter.rus {
				letLat := []rune(letter.lat)
				transl = append(transl, letLat...)
			}
		}
	}
	return string(transl[len(data):])
}

func translFileToLatin() {
	data, err := os.ReadFile("example.txt")
	check(err)

	translStr := translRusToEn(string(data))

	f, err := os.OpenFile("example.processed.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0600)
	check(err)
	defer f.Close()

	_, err = f.WriteString(translStr)
	check(err)
}
