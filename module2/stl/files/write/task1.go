package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter your name: ")

	name, _ := reader.ReadString('\n')
	fmt.Printf("Hello %s\n", name)

	err := os.WriteFile("task_file.txt", []byte(name), 0600)
	if err != nil {
		fmt.Println(err)
	}
	translFileToLatin()
}
