package main

import (
	"bytes"
	"fmt"
	"io"
	"os"
)

func main() {
	data := []string{
		"there is 3pm, but im still alive to write this code snippet",
		"чистый код лучше, чем заумный код",
		"ваш код станет наследием будущих программистов",
		"задумайтесь об этом",
	}

	// здесь расположите буфер
	buffer := new(bytes.Buffer)

	// запишите данные в буфер
	for _, s := range data {
		buffer.WriteString(s + "\n")
	}

	// создайте файл
	file, err := os.Create("example.processed.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	// запишите данные в файл
	if _, err := io.Copy(file, buffer); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer file.Close()

	// прочтите данные в новый буфер
	bufferNew := new(bytes.Buffer)
	file, err = os.Open("example.processed.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer file.Close()

	if _, err := io.Copy(bufferNew, file); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	fmt.Println(bufferNew.String())

}
