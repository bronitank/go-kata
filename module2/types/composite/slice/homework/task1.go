package main

import "fmt"

func addNumInSlice() {
	s := Append([]int{1, 2, 3})
	fmt.Println(s)

	s2 := make([]int, 3, 4)
	for i := 0; i < 3; i++ {
		s2[i] = s[i]
	}
	Append2(&s2)
	fmt.Println(s2)
}

func Append(s []int) []int {
	s = append(s, 4)
	return s
}

func Append2(s *[]int) {
	*s = append(*s, 4)
}
