package main

import (
	"fmt"
	"unsafe"
)

// Объявление пользовательского типа
type User struct {
	Age      int
	Name     string
	Wallet   Wallet   // добавим поле Wallet
	Location Location // добавим поле Location
}

type Location struct {
	Address string
	City    string
	Index   string
}

// Wallet структура кошелька пользователя
type Wallet struct {
	RUR uint64
	USD uint64
	BTC uint64
	ETH uint64
}

func main() {
	user := User{
		Age:  13, // звполняем поля структуры
		Name: "Alexsander",
	}
	fmt.Println(user)
	wallet := Wallet{
		RUR: 250000,
		USD: 3500,
		BTC: 1,
		ETH: 4,
	}
	fmt.Println(wallet)
	fmt.Println("wallet allocates: ", unsafe.Sizeof(wallet), "bytes") // вывод занимаемоц памяти структурой Wallet
	user.Wallet = wallet
	fmt.Println(user)

	user2 := User{
		Age:  34,
		Name: "Anton",
		Wallet: Wallet{
			RUR: 144000,
			USD: 8900,
			BTC: 55,
			ETH: 34,
		},
		Location: Location{
			Address: "Нововатутинская 3-Я ул,sy 13, к.2",
			City:    "Москва",
			Index:   "108836",
		},
	}
	fmt.Println(user2)
}
