package main

import "fmt"

type Project struct {
	Name  string
	Stars int
}

func main() {
	projects := []Project{
		{
			Name:  "https://github.com/docker/compose",
			Stars: 27600,
		},
		{
			Name:  "https://github.com/docker/buildx",
			Stars: 2421,
		},
		{
			Name:  "https://github.com/docker/containerd-packaging",
			Stars: 52,
		},
		{
			Name:  "https://github.com/docker/docs",
			Stars: 3753,
		},
		{
			Name:  "https://github.com/docker/packaging",
			Stars: 17,
		},
		{
			Name:  "https://github.com/docker/docker-py",
			Stars: 6114,
		},
		{
			Name:  "https://github.com/docker/compose-cli",
			Stars: 906,
		},
		{
			Name:  "https://github.com/docker/scan-cli-plugin",
			Stars: 154,
		},
		{
			Name:  "https://github.com/docker/index-cli-plugin",
			Stars: 39,
		},
		{
			Name:  "https://github.com/docker/build-push-action",
			Stars: 3073,
		},
		{
			Name:  "https://github.com/docker/buildkit-syft-scanner",
			Stars: 11,
		},
		{
			Name:  "https://github.com/docker/cli",
			Stars: 3868,
		},
		{
			Name:  "https://github.com/docker/awesome-compose",
			Stars: 21346,
		},
	}
	structdocker := map[string]Project{}
	for _, value := range projects {
		structdocker[value.Name] = value
	}
	for _, mapvalue := range structdocker {
		fmt.Println(mapvalue)
	}
}
