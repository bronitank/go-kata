package main

import (
	"fmt"
	"unsafe"
)

func main() {
	n := 112358132134
	fmt.Println("size is:", unsafe.Sizeof(n), "bytes")
	typeBool()
}

func typeBool() {
	var b bool
	fmt.Println("Размер в байтиах:", unsafe.Sizeof(b)) // размер в байтах: 1
	var u uint8 = 1                                    // Берём uint8 - 8 бит, выставляем значение 1
	fmt.Println(b)                                     // bool  по умолчанию is false
	b = *(*bool)(unsafe.Pointer(&u))                   // берём значение из uint8 проставим в тип bool
	fmt.Println(b)                                     // Распечатаем значение TRUE
}
