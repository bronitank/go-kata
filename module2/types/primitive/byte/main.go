package main

import (
	"fmt"
	"unsafe"
)

func main() {
	n := 112358132134
	fmt.Println("size is:", unsafe.Sizeof(n), "bytes")
	typeByte()
}

func typeByte() {
	var b byte = 124
	fmt.Println("Размер в байтах:", unsafe.Sizeof(b)) // размер в байтах: 1
}
