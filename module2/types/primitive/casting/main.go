package main

import "fmt"

func main() {
	var numberInt int = 3 // объявим переменную(объект) типа integer
	var numberFloat = float32(numberInt)
	fmt.Printf("тип: %T, значение: %v", numberFloat, numberFloat)
}
