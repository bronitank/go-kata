package main

import (
	"fmt"
)

type MyInterface interface{}

func main() {
	var n *int
	fmt.Println(n == nil)
	test(n)
}

func test(r interface{}) {
	switch r := r.(type) {
	case *int:
		fmt.Println("Success!", r)
	}
}
